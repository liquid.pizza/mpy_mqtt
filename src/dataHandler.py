import ujson

# read/write helper
CONFIG_FILENAME = "config.json"

MQTT_CONFIG_KEY = "mqtt"

MQTT_BROKER_KEY = "broker"
MQTT_TOPIC_KEY = "topic"

WIFI_CONFIG_KEY = "wifi"

WIFI_SSID_KEY = "ssid"
WIFI_PASS_KEY = "pass"

def readJson(filename = CONFIG_FILENAME):
    with open(filename) as f:
        print(filename, "read.")
        return ujson.load(f)

def writeJson(data, filename = CONFIG_FILENAME):
    with open(filename, "w") as f:
        ujson.dump(data, f)
        print(filename, "written.")

config = readJson()
mqtt_config = config[MQTT_CONFIG_KEY]
wifi_config = config[WIFI_CONFIG_KEY]

configIsUpdated = False
lastMode = None
