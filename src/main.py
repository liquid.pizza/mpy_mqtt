import machine
import ubinascii
import network
from umqtt.simple import MQTTClient

from dataHandler import \
    mqtt_config, wifi_config,\
    MQTT_BROKER_KEY, MQTT_TOPIC_KEY,\
    WIFI_SSID_KEY, WIFI_PASS_KEY

# Default MQTT server to connect to
MQTT_CLIENT_ID = ubinascii.hexlify(machine.unique_id())
MQTT_BROKER = mqtt_config[MQTT_BROKER_KEY]
MQTT_TOPIC = mqtt_config[MQTT_TOPIC_KEY]

WIFI_SSID = wifi_config[WIFI_SSID_KEY]
WIFI_PASS = wifi_config[WIFI_PASS_KEY]

def handle_msg(msg):
    print("\tReceived '%s'" % (msg))

def sub_cb(topic, msg):
    # `topic` and `msg` are byte arrays and have to be decoded (`decode()`)

    # split topic
    t = topic.decode().split("/")
    handle_msg(msg.decode())

def connect_wifi(
    wifi_ssid=WIFI_SSID,
    wifi_pass=WIFI_PASS,
    ):
    print("Connecting to %s" % (wifi_ssid))

    station = network.WLAN(network.STA_IF)

    station.active(True)
    station.connect(
        wifi_ssid,
        wifi_pass
        )

    while station.isconnected() == False:
        pass

    print('Connection successful!')
    print(station.ifconfig())

def setup_mqtt(
    mqtt_broker=MQTT_BROKER,
    mqtt_topic=MQTT_TOPIC,
    mqtt_client=MQTT_CLIENT_ID,
    ):

    # connect to the broker
    print("Connecting to %s as %s" % (mqtt_broker, mqtt_client))
    mqttClient = MQTTClient(mqtt_client, mqtt_broker)
    print("Connected to broker.")

    # Subscribed messages will be delivered to this callback
    mqttClient.set_callback(sub_cb)
    mqttClient.connect()

    # subscribe to the topic
    print("Subscribing to topic %s@%s" % (mqtt_topic, mqtt_broker))
    mqttClient.subscribe(mqtt_topic)
    print("Subscribtion successful!")

    # loop it
    while 1:
        mqttClient.wait_msg()

def main():
    print("Starting...")

    connect_wifi()
    setup_mqtt()

if __name__ == "__main__":
    main()