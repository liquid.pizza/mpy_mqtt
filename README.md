# mpy_mqtt

Simple [MicroPython](https://micropython.org/) [MQTT](https://mqtt.org/)-client.

## Setup

1. Change the `src/config.json` to match your needs.
1. Flash all scripts to the microcontroller: `ampy -p <port> put *`
1. Connect to the devices port: `screen <port> <baut_rate>` - _The default baut rate is 115200_
